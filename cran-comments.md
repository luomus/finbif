## Resubmission

* Removed redundant references to 'R' in DESCRIPTION.
* Fixed link in description field. Now in "<https://...>" form.
* Software names now in single quotes.
* 'cph' added to Authors@R
* Removed redundant '-' in LICENSE file

## Test environments

* local: Linux5.0.0-32-generic
* travis: oldrel, release, release (osx), devel
* appveyor: oldrel, stable, patched
* r-hub: windows-x86_64-devel, ubuntu-gcc-release, fedora-clang-devel
* win-builder: windows-x86_64-devel

## R CMD check results
0 errors | 0 warnings
